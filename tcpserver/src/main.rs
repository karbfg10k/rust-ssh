use std::net::{TcpListener, TcpStream};
use std::io::{Read, Write};

fn handle_client(mut stream: TcpStream) {
    // ...
    let peer = stream.peer_addr().ok().expect("stream failed");
    println!("new client: {}:{}", peer.ip(), peer.port());
    let mut buf: [u8; 1024] = [0; 1024];

    let bytes_recv = stream.read(&mut buf).ok().expect("Couldn't receive from client"); 
    let s = String::from_utf8_lossy(&buf);
    println!("received {} bytes: {}", bytes_recv, s);

    let bytes_sent = stream.write(&buf).ok().expect("Couldn't send to client");
    let _ = stream.flush();
    println!("sent {} bytes", bytes_sent);
}


fn main() {
    let listener = TcpListener::bind("127.0.0.1:8022").ok().expect("Bind fail");

    let addr = listener.local_addr().ok().expect("Listener fail");

    println!("listening on {}, port {}...", addr.ip(), addr.port());

    // accept connections and process them serially
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                handle_client(stream);
            }
            Err(e) => { 
                // connection failed
                println!("Error: {}", e)
            }
        }
    }
}
