use std::net::TcpStream;
use std::io::{Read, Write};

fn main() {
    //Use .unwrap() if you don't want to take care of errors
    let mut stream = TcpStream::connect("127.0.0.1:8022").ok().expect("Failed at Tcp connect"); 
    println!("Connected to the server!");
    let bytes_sent = stream.write(b"hello").ok().expect("Couldn't send to server");
    let _ = stream.flush();
    println!("sent: {} bytes", bytes_sent);

    let mut buf: [u8; 1024] = [0; 1024];
    let bytes_recv = stream.read(&mut buf).ok().expect("Couldn't receive from server"); 
    let s = String::from_utf8_lossy(&buf);
    println!("received {} bytes: {}", bytes_recv, s);    
} 
